# Neon Oasis

In the middle of a rocky badland lies a shining oasis, a neon-lit jewel. The city of Oasis, an arcology rising from the desert to touch the heavens. Explore the dark mysteries of this city in the RPG Maker game, Neon Oasis.